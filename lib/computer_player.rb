class ComputerPlayer
  attr_reader :name
  attr_accessor :board , :mark

  def initialize(name)
    @name = name
    @board = nil
    @mark = nil
    @computer_move = nil
  end

  def display(board)
    @board = board
  end

  def get_move
     board.grid.each_with_index do |row_num, row_index|
       row_num.each_with_index do |column_in_row_ele, column_in_row_index|
         break unless @computer_move.nil?
         move = [column_in_row_index, row_index]
         @computer_move = move if winning_move?(move)
       end
     end
    @computer_move = [rand(3) ,rand(3)] if @computer_move.nil?
    @computer_move
  end

  def winning_move?(move)
    winner = false
    if board.empty?(move)
       board.place_mark(move, mark)
        if board.winner != nil
          winner = true
        end
       board.place_mark(move, nil)
    end
    winner
  end
end
