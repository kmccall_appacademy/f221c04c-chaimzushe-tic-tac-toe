require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :current_player, :player_one, :player_two
  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  def play
    until board.over?
      puts board.grid
      play_turn
    end
  end

  def play_turn
    move = current_player.get_move
    board.place_mark(move, current_player.mark)
    switch_players!
  end

  def switch_players!
    case current_player
    when player_one
      self.current_player = player_two
    when player_two
      self.current_player = player_one
    end
  end

end
