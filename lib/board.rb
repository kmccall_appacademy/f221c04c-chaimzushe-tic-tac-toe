class Board

  attr_reader :grid
  def initialize(grid=nil)
    @grid = grid.nil? ? [ [nil,nil,nil], [nil,nil,nil], [nil,nil,nil] ] : grid
  end

  def place_mark(pos, symbol)
    raise "you may not place a #{symbol} in #{pos}. the spot is taken" if !empty?(pos) && !symbol.nil?
    x, y = pos
    grid[x][y] = symbol
  end

  def empty?(pos)
    x, y = pos
    grid[x][y].nil?
  end

  def winner
      if  row_winner(:X) || diagnol_winner(:X) || column_winner(:X)
        return :X
      elsif row_winner(:O) || diagnol_winner(:O) || column_winner(:O)
        return :O
      end
  end


  def diagnol_winner(symbol)
     return (grid[0][0] == symbol && grid[1][1] == symbol && grid[2][2] == symbol) ||
     (grid[0][2] == symbol && grid[1][1] == symbol && grid[2][0] == symbol)
  end


  def row_winner(symbol)
    grid.any?{ |row| row == [symbol , symbol, symbol] }
  end

  def column_winner(symbol)
    (grid[0][0] == symbol && grid[1][0] == symbol && grid[2][0] == symbol) ||
    (grid[0][1] == symbol && grid[1][1] == symbol && grid[2][1] == symbol) ||
    (grid[0][2] == symbol && grid[1][2] == symbol && grid[2][2] == symbol)
  end

  def over?
    return winner || grid.flatten.none?{|ele| ele.nil?}
  end

end
